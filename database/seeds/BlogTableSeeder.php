<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => 'MENJAGA',
            'content' => 'MENJAGA
            November 12, 2018 1 Comment
            "SIAPA ORANG YANG SERING KITA BAWA KE KAMPUS?" Aku berteriak memulai tebak-tebakan sembari membawa mobil. "YAK BETUL! ALMAMOTHER HAHAHA!"

            "YEEE ANJ!!! HAHAHA" Balas temanku kesal.

            Hari ini adalah hari terakhir dia di Bekasi, liburan kuliahnya sudah berakhir, dan kami -Aku dan Kekasih Temanku- bertugas untuk menemaninya ke stasiun. Tradisi lah bisa dibilang. Setiap kali ia kesini, aku seperti punya kewajiban untuk menemani kepulangan dan kepergiannya.

            Aku melajukan mobilku masuk ke arah stasiun, dan berhenti tepat di depan lokasi parkir.

            Mereka berdua -Temanku dan Kekasihnya- duduk di bangku belakang. Sebagai teman yang baik, aku membiarkan mereka melakukan apapun. Sekedar peluk cium kan memang sudah biasa saat ingin berpisah. Apalagi mereka LDR, biarkanlah mereka bercengkrama, menghilangkan rindu yang menggebu.

            "Kamu waktu aku tinggal jangan nakal ya. Main sama cowok boleh, tapi jangan kelewatan. Nanti aku cemburu, bisa-bisa malem itu juga aku bisa di depan rumah kamu cuma buat sekedar ngambek minta peluk." Kata temanku sambil memeluk kekasihnya itu erat.

            "Iya, sayang. Percaya deh, aku nggak bakal nakal." Jawab kekasihnya berusaha meyakinkan.

            "Nanti kalo kamu butuh apa-apa, minta tolong Andri aja ya. Nanti aku suruh dia jagain kamu." Begitu katanya sebelum mendaratkan ciuman perpisahan, dan melangkah pergi ke arah peron kereta.

            Aku pun menyalakan mesin mobil, dan perlahan meninggalkan area stasiun.

            "Udah tinggal berdua nih, nonton dulu apa hotel dulu?" tanyaku.

            "Hotel aja lah. Ga tahan." Begitu katanya, sambil mendaratkan kecup pada pipiku.

            Yah begitulah. Seperti kata temanku, aku memang menjaganya.

            Dari kekasihnya sendiri.

            —lunatictwister
            #AksaraSelepasSenja'
        ]);
    }
}
